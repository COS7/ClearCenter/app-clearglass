<?php

/**
 * ClearGLASS settings view.
 *
 * @category   apps
 * @package    clearglass
 * @subpackage view
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearglass/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('clearglass');

///////////////////////////////////////////////////////////////////////////////
// Form handler
///////////////////////////////////////////////////////////////////////////////

if ($init) {
    $read_only = FALSE;
    $buttons = array(
        form_submit_update('submit'),
    );
} else if ($form_type === 'edit') {
    $read_only = FALSE;
    $buttons = array(
        form_submit_update('submit'),
        anchor_cancel('/app/' . $app_name . '/settings'),
    );
} else {
    $read_only = TRUE;
    $buttons = array(
        anchor_edit('/app/' . $app_name . '/settings/edit')
    );
}

///////////////////////////////////////////////////////////////////////////////
// Init helper
///////////////////////////////////////////////////////////////////////////////

if ($init)
    echo infobox_info(lang('base_information'), lang('clearglass_initialization_help'));

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

echo form_open($app_name . '/settings/edit');
echo form_header(lang('base_settings'));

echo field_dropdown('hostname', $hostnames, $hostname, lang('base_secure_hostname'), $read_only);
echo field_button_set($buttons);

echo form_footer();
echo form_close();
