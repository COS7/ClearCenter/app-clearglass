<?php

$lang['clearglass_app_description'] = 'The ClearGLASS app provides the base engine for the Community and Business editions.';
$lang['clearglass_app_name'] = 'ClearGLASS';
$lang['clearglass_go_to_clearglass'] = 'Go To ClearGLASS';
$lang['clearglass_initialization_help'] = 'Please select the secure hostname that will be used to access the ClearGLASS web portal.';
$lang['clearglass_clearglass_web_access_help'] = 'Follow the link to access the ClearGLASS web-based interface.';
$lang['clearglass_limited_warning'] = 'Sorry, ClearGLASS requires at least 7 GB of memory.';
