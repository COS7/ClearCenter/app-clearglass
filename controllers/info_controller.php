<?php

/**
 * ClearGLASS info controller.
 *
 * @category   apps
 * @package    clearglass
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearglass/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearGLASS info controller.
 *
 * @category   apps
 * @package    clearglass
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/clearglass/
 */

class Info_Controller extends ClearOS_Controller
{
    protected $app_name = NULL;

    /**
     * ClearGLASS info constructor.
     *
     * @param string $app_name app that manages the docker app
     *
     * @return view
     */

    function __construct($app_name)
    {
        $this->app_name = $app_name;
    }

    /**
     * ClearGLASS info controller
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->load->library('clearglass/ClearGLASS');
        $this->lang->load('clearglass');
        $this->lang->load('base');

        // Load view data
        //---------------

        $data['app_name'] = $this->app_name;

        try {
            $data['url'] = $this->clearglass->get_url($_SERVER['REMOTE_ADDR'], $_SERVER['SERVER_NAME']);
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $this->page->view_form('clearglass/info', $data, lang('base_info'));
    }
}
