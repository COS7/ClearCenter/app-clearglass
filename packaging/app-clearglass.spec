
Name: app-clearglass
Epoch: 1
Version: 2.5.27
Release: 1%{dist}
Summary: ClearGLASS
License: GPLv3
Group: Applications/Apps
Packager: ClearFoundation
Vendor: ClearFoundation
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base
Requires: app-network
Requires: app-docker

%description
The ClearGLASS app provides the base engine for the Community and Business editions.

%package core
Summary: ClearGLASS - API
License: LGPLv3
Group: Applications/API
Requires: app-base-core
Requires: app-base-core >= 1:2.4.20
Requires: app-docker-core
Requires: app-events-core
Requires: app-mail-routing-core
Requires: app-network-core
Requires: clearglass-engine
Requires: /usr/lib/systemd/system/clearglass.service

%description core
The ClearGLASS app provides the base engine for the Community and Business editions.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/clearglass
cp -r * %{buildroot}/usr/clearos/apps/clearglass/

install -d -m 0755 %{buildroot}/var/clearos/clearglass
install -d -m 0755 %{buildroot}/var/clearos/clearglass/backup
install -D -m 0755 packaging/clearglass.letsencrypt %{buildroot}/var/clearos/events/lets_encrypt/clearglass
install -D -m 0755 packaging/network-configuration-event %{buildroot}/var/clearos/events/network_configuration/clearglass
install -D -m 0755 packaging/onboot-event %{buildroot}/var/clearos/events/onboot/clearglass

%post
logger -p local6.notice -t installer 'app-clearglass - installing'

%post core
logger -p local6.notice -t installer 'app-clearglass-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/clearglass/deploy/install ] && /usr/clearos/apps/clearglass/deploy/install
fi

[ -x /usr/clearos/apps/clearglass/deploy/upgrade ] && /usr/clearos/apps/clearglass/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-clearglass - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-clearglass-api - uninstalling'
    [ -x /usr/clearos/apps/clearglass/deploy/uninstall ] && /usr/clearos/apps/clearglass/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/clearglass/controllers
/usr/clearos/apps/clearglass/htdocs
/usr/clearos/apps/clearglass/views

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/clearglass/packaging
%exclude /usr/clearos/apps/clearglass/unify.json
%dir /usr/clearos/apps/clearglass
%dir /var/clearos/clearglass
%dir /var/clearos/clearglass/backup
/usr/clearos/apps/clearglass/deploy
/usr/clearos/apps/clearglass/language
/usr/clearos/apps/clearglass/libraries
/var/clearos/events/lets_encrypt/clearglass
/var/clearos/events/network_configuration/clearglass
/var/clearos/events/onboot/clearglass
